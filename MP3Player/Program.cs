﻿using NAudio.Wave;

// Load tất cả các file .mp3 lên sau đó chạy từng bài theo thứ tự
var mp3Player = new MP3Player();
mp3Player.PlayMusic("UngQuaChung-AMEE-8783624.mp3");
mp3Player.PlayMusic("TinhYeuKhungLong-FAY-6247040.mp3");

Console.WriteLine("All music playback finished.");


class MP3Player
{
    public void PlayMusic(string filePath)
    {
        var audioFile = new AudioFileReader(filePath);
        var outputDevice = new WaveOutEvent();
        outputDevice.Init(audioFile);
        outputDevice.Play();
        Console.WriteLine("Music is playing: " + filePath);

        while (outputDevice.PlaybackState == PlaybackState.Playing)
        {
            // Đợi đến khi bài hát hoàn thành
        }

        Console.WriteLine("Audio playback finished: " + filePath);
    }
}